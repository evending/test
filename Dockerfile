FROM glowroot/glowroot-central

RUN groupadd -r glowroot && useradd --no-log-init -r -g glowroot glowroot

USER glowroot:glowroot

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["java", "-jar", "glowroot-central.jar"]
